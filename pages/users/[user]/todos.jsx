import axios from 'axios'
import { useRouter } from 'next/router'
import React from 'react'

const TodosPage = ({ todos, user }) => {
  const router = useRouter()

  return (
    <div className='flex items-start justify-center w-screen h-screen mt-8'>
      <div className='flex flex-col'>
        <div className='flex gap-3 mb-4'>
          <button onClick={() => router.push('/')} className='text-xs'>
            Home
          </button>
          <button onClick={() => router.back()} className='text-xs'>
            Go Back
          </button>
        </div>
        <p className='flex items-center gap-4 mb-6 text-sm font-semibold'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='w-6 h-6'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2'
            />
          </svg>
          Tasks for {user.name}
        </p>
        <ul className='flex flex-col gap-4'>
          {todos.map((todo) => (
            <li className='flex items-center gap-4 text-sm' key={todo.id}>
              <button className='p-1 border-2 border-green-300 rounded-full hover:bg-green-300 group'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='w-4 h-4 text-green-500 group-hover:text-white'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                >
                  <path strokeLinecap='round' strokeLinejoin='round' strokeWidth={2} d='M5 13l4 4L19 7' />
                </svg>
              </button>
              {todo.title}
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export async function getStaticPaths() {
  const users = await axios.get('https://jsonplaceholder.typicode.com/users')
  const paths = users.data.map((user) => {
    return {
      params: {
        user: String(user.id),
      },
    }
  })
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps(context) {
  const { params } = context
  const todos = await axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${params.user}`)
  const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${params.user}`)
  if (!todos.data || !user.data) {
    return {
      notFound: true,
    }
  }
  return {
    props: {
      todos: todos.data,
      user: user.data,
    },
  }
}

export default TodosPage
