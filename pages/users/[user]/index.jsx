import React from 'react'
import Link from 'next/link'
import axios from 'axios'
import { useRouter } from 'next/router'

const UserDetailsPage = ({ user }) => {
  const router = useRouter()
  return (
    <div className='flex flex-col items-center justify-center w-screen h-screen'>
      <div className='flex gap-4'>
        <Link href='/'>
          <a className='flex items-center gap-2 p-2 mb-8 font-semibold text-blue-500 rounded-lg hover:bg-blue-100'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              className='w-5 h-5'
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth={2}
                d='M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6'
              />
            </svg>
            Home
          </a>
        </Link>
        <button
          className='flex items-center gap-2 p-2 mb-8 font-semibold text-blue-500 rounded-lg hover:bg-blue-100'
          onClick={() => router.back()}
        >
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='w-5 h-5'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z'
            />
          </svg>
          Go back
        </button>
      </div>
      <div className='h-auto p-4 border-2 border-gray-200 rounded-xl bg-gray-50'>
        <p className='flex items-center gap-2 mb-3 font-bold text-gray-600'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='w-6 h-6'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2'
            />
          </svg>
          User Info
        </p>
        {/* name */}
        <p className='flex items-center gap-2 mb-3 text-sm font-medium text-gray-500'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='w-5 h-5'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z'
            />
          </svg>
          {user.name}
        </p>
        {/* phone */}
        <p className='flex items-center gap-2 mb-3 text-sm font-medium text-gray-500'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='w-5 h-5'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z'
            />
          </svg>
          {user.phone}
        </p>
        {/* todo button */}
        <div className='mt-10'>
          <Link href={`${router.asPath}/todos`}>
            <a className='flex items-center justify-center gap-2 p-2 text-sm font-medium text-white uppercase bg-blue-500 rounded-lg hover:bg-blue-600'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='w-6 h-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth={2}
                  d='M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01'
                />
              </svg>
              View Todos
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export async function getStaticPaths() {
  const response = await fetch('https://jsonplaceholder.typicode.com/users')
  const data = await response.json()
  const paths = data.map((user) => {
    return {
      params: { user: String(user.id) },
    }
  })
  return { paths, fallback: false }
}

export async function getStaticProps(context) {
  const { params } = context
  const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${params.user}`)
  return {
    props: {
      user: user.data,
    },
  }
}

export default UserDetailsPage
