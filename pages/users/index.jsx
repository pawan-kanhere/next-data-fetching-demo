import React from 'react'
import axios from 'axios'
import Link from 'next/link'

const UserPage = ({ data }) => {
  return (
    <div className='flex flex-col items-center justify-center w-screen h-screen m-4'>
      <div className='flex items-center gap-2 mb-6'>
        <input
          value={user}
          onChange={(evt) => setUser(evt.target.value)}
          className='p-2 pl-3 border-2 rounded-lg outline-none placeholder-shown:pl-3 focus-within:ring-2 focus-within:border-transparent'
          placeholder='Add User'
        />
        <button onClick={() => handleClick(user)} className='px-2 py-1 font-semibold rounded-lg hover:bg-gray-100'>
          Add
        </button>
      </div>
      <h5 className='mb-8 text-xl font-semibold text-gray-500 uppercase'>List of users</h5>
      <div className='grid grid-cols-3 gap-4 mb-16'>
        {data.map((user) => (
          <Link key={user.id} href={`/users/${user.id}`}>
            <a className='flex gap-2 p-4 text-gray-600 border-2 w-72 rounded-xl bg-gray-50 hover:bg-gray-200'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='w-6 h-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth='2'
                  d='M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z'
                />
              </svg>
              {user.name}
            </a>
          </Link>
        ))}
      </div>
    </div>
  )
}

export async function getStaticProps() {
  const response = await axios.get('https://jsonplaceholder.typicode.com/users')
  if (!response.data) {
    return {
      notFound: true,
    }
  }
  return {
    props: { data: response.data },
  }
}
export default UserPage
