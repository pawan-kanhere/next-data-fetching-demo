import React from 'react'
import Link from 'next/link'

const IndexPage = () => {
  return (
    <div className='w-screen h-screen p-4'>
      <div className='flex gap-3'>
        <Link href='/users'>
          <a className='flex items-center justify-center gap-2 p-4 text-sm font-bold tracking-normal text-gray-500 uppercase border-2 border-gray-300 w-52 rounded-2xl hover:bg-gray-100'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              className='w-5 h-5 text-gray-500'
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth='2'
                d='M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z'
              />
            </svg>
            Users
          </a>
        </Link>
      </div>
    </div>
  )
}

export default IndexPage
